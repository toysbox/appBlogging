# Python Flask

## Overview

Please, also take a look on our [README](https://gitlab.com/toysbox/appBlogging/blob/master/README.md).

## Docker Flask (python) fidings

This is the first version (v0.1.0)

- if you have `docker swarm` running:  
  `docker service create -p 80:80 --replicas 3 --name appBlogging registry.gitlab.com/toysbox/appBlogging`

***or***

- *"standalone"*:  
  `docker run -d -p 80:80 --name appBlogging registry.gitlab.com/toysbox/appBlogging`

 You could use some Flask environment variable to customize something  
 (e.g `docker run --env FLASK_DEBUG=1 -d -p 80:80 --name appBlogging registry.gitlab.com/toysbox/appBlogging`).

Source code: <https://gitlab.com/toysbox/appBlogging>

## Tags

- **standard and supported**:
  - **centos7** (***latest***): built based on centos:7 image (*[Dockerfile](https://gitlab.com/toysbox/appBlogging/blob/master/Dockerfile)*)

----

***keep CALMS and having fun***
