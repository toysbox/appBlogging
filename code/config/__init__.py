import logging


USERNAME = 'admin'
PASSWORD = 'secret'
SECRET_KEY = 'token-devel'

SQLALCHEMY_DATABASE_URI = 'sqlite:////tmp/minitwit.db'
SQLALCHEMY_TRACK_MODIFICATIONS = True

DEBUG = True
DEBUG_LEVEL = logging.DEBUG
LOGGING_FORMAT = '%(asctime)-15s %(clientip)s %(user)-8s %(message)s'
