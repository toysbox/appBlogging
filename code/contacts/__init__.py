# -*- coding: UTF-8 -*-
'''
Application: Blogging
Modulue: application starting point
description: Tasting docker, redis, kafka, rancher
author: Adriano dos Santos Vieira <adriano.svieira at google.com>
character encoding: UTF-8

Copyright 2016 Adriano dos Santos Vieira

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
@license @see LICENCE
'''
import sys
import os


sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)))
sys.path.append(os.path.dirname(os.path.abspath(__file__)))
reload(sys)
sys.setdefaultencoding('UTF-8')


from contacts import app as application
