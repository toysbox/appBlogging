# -*- coding: UTF-8 -*-
'''
Application: Blogging
Modulue: contacts
description: Tasting docker, redis, kafka, rancher
author: Adriano dos Santos Vieira <adriano.svieira at google.com>
character encoding: UTF-8
'''

import os
import time
import sqlite3
from flask import Flask, request, session, g, redirect, url_for, abort, \
     render_template, flash


app = Flask(__name__)
app.config.from_object(__name__)

app.config.update(dict(
    DATABASE=os.path.join('/tmp/', 'contacts.db'),
    SECRET_KEY='token-devel',
    USERNAME='admin',
    PASSWORD='secret'
))
app.config.from_envvar('APPHELLO_CONTACTS_SETTINGS', silent=True)


def connect_db():
    """Connects to the specific database."""
    rv = sqlite3.connect(app.config['DATABASE'])
    rv.row_factory = sqlite3.Row
    return rv


def init_db():
    db = get_db()
    with app.open_resource('sql/schema.sql', mode='r') as f:
        db.cursor().executescript(f.read())
    db.commit()


@app.cli.command('initdb')
def initdb_command():
    """Initializes the database."""
    init_db()
    print('Initialized the database.')


def get_db():
    """Opens a new database connection if there is none yet for the
    current application context.
    """
    if not hasattr(g, 'sqlite_db'):
        g.sqlite_db = connect_db()
    return g.sqlite_db


@app.teardown_appcontext
def close_db(error):
    """Closes the database again at the end of the request."""
    if hasattr(g, 'sqlite_db'):
        g.sqlite_db.close()


@app.route("/")
def index():
    return render_template('index.html')


@app.route('/contacts', methods=['GET'])
def show_contacts():
    db = get_db()
    try:
        cur = db.execute('select title, text from contacts order by id desc')
    except:
        init_db()

    cur = db.execute('select title, text from contacts order by id desc')
    contacts = cur.fetchall()
    return render_template('show_contacts.html', contacts=contacts)


@app.route('/contacts', methods=['POST'])
def add_entry():
    """if not session.get('logged_in'):
        abort(401)"""
    db = get_db()
    db.execute('insert into contacts (title, text) values (?, ?)',
               [request.form['title'], request.form['text']])
    db.commit()
    flash('New entry was successfully posted')
    return redirect(url_for('show_contacts'))


@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        if request.form['username'] != app.config['USERNAME']:
            error = 'Invalid username'
        elif request.form['password'] != app.config['PASSWORD']:
            error = 'Invalid password'
        else:
            session['logged_in'] = True
            flash('You were logged in')
            return redirect(url_for('index'))
    return render_template('login.html', error=error)


@app.route('/logout')
def logout():
    session.pop('logged_in', None)
    flash('You were logged out')
    return redirect(url_for('index'))


@app.route("/about")
def about():
    try:
        f = open(os.path.dirname(os.path.abspath(__file__))+'/../VERSION')
        app_version = f.readline()
        f.close()
    except IOError as e:
        app_version = 'unreleased developement'

    return render_template('about.html',
                           APP_VERSION=app_version,
                           datetime=time.strftime("%d/%h/%Y %H:%M:%S"),
                           container=os.uname()[1],
                           hosted=' '.join(os.uname()))
