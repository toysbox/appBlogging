import os
import unittest
import tempfile
from contacts import contacts

class ContactsTestCase(unittest.TestCase):

    def setUp(self):
        self.db_fd, contacts.app.config['DATABASE'] = tempfile.mkstemp()
        contacts.app.config['TESTING'] = True
        self.app = contacts.app.test_client()
        with contacts.app.app_context():
            contacts.init_db()

    def tearDown(self):
        os.close(self.db_fd)
        os.unlink(contacts.app.config['DATABASE'])

    def test_empty_db(self):
        rv = self.app.get('/')
        assert b'App Under Construction' in rv.data

    def login(self, username, password):
        return self.app.post('/login', data=dict(
            username=username,
            password=password
        ), follow_redirects=True)

    def logout(self):
        return self.app.get('/logout', follow_redirects=True)

    def test_login_logout(self):
        rv = self.login('admin', 'secret')
        assert b'You were logged in' in rv.data
        rv = self.logout()
        assert b'You were logged out' in rv.data
        rv = self.login('adminx', 'default')
        assert b'Invalid username' in rv.data
        rv = self.login('admin', 'defaultx')
        assert b'Invalid password' in rv.data

    def test_messages(self):
        self.login('admin', 'secret')
        rv = self.app.post('/contacts', data=dict(
            title='userpyttest',
            text='userpyttest data allowed here'
        ), follow_redirects=True)
        assert b'No contacts here so far' not in rv.data
        assert b'userpyttest' in rv.data
        assert b'userpyttest data allowed here' in rv.data

if __name__ == '__main__':
    unittest.main()
