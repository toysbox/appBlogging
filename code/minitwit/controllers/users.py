# -*- coding: UTF-8 -*-
'''
Application: Blogging
Module: Users' Controller
Description: Tasting Docker, Flask, Rancher
author: Adriano dos Santos Vieira <adriano.svieira at google.com>
character encoding: UTF-8

Copyright 2016 Adriano dos Santos Vieira

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
@license @see LICENCE
'''
from flask import Flask, render_template, json, jsonify, \
                  Response, make_response, request, session, g, abort
from minitwit import app, db
from minitwit.models.tables import User


# RESTFULL
# https://en.wikipedia.org/wiki/Representational_state_transfer#Relationship_between_URL_and_HTTP_methods
@app.route("/users", methods=['GET'])
def users_show():
    users = User.query.all()
    return render_template('users_show.html', users=users)


@app.route("/users/<username>", methods=['GET'])
def users_get(username):
    resp = make_response(jsonify(status=False,
                                 origin=request.headers.get(
                                     'X-Forwarded-For',
                                     request.remote_addr)), 503)
    try:
        user = User.query.filter_by(username=username).first()
        resp = jsonify(username=user.username, name=user.name,
                       email=user.email, id=user.id)
    except Exception as e:
        raise
    finally:
        return resp


@app.route("/users/<username>", methods=['POST'])
def users_add(username):
    resp = jsonify(status=False, origin=request.remote_addr)
    resp.status_code = 503
    if not session.get('logged_in'):
        resp.status_code = 401
        return resp

    try:
        if request.headers['Content-Type'] == 'application/json':
            data = request.get_json()

        # username, password, name, email
        user = User(username, data['password'], data['name'],
                    data['email'])
        db.session.add(user)
        db.session.commit()
        resp = make_response(
            jsonify(status='OK', message='User %s added' % username,
                    origin=request.headers.get(
                        'X-Forwarded-For', request.remote_addr)), 200)

    except Exception as e:
        resp.status_code = 403
        app.logger.debug("message: %s.", e)
        raise
    finally:
        return resp


@app.route("/users/<username>", methods=['PUT'])
def users_update(username):
    # username, password, name, email
    user = User('admin', 'senha', 'Administrador', 'admin@example.com')
    db.session.update(user)
    db.session.commit()
    return "Users post/add!"


@app.route("/users/<username>", methods=['DELETE'])
def users_delete(username):
    # username, password, name, email
    user = User('admin', 'senha', 'Administrador', 'admin@example.com')
    db.session.delete(user)
    db.session.commit()
    return "Users delete user!"
