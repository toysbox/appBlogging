# -*- coding: UTF-8 -*-
'''
Application: Blogging
Module: Controllers Routes
Description: Tasting Docker, Flask, Rancher
author: Adriano dos Santos Vieira <adriano.svieira at google.com>
character encoding: UTF-8

Copyright 2016 Adriano dos Santos Vieira

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
@license @see LICENCE
'''
import os
import time
from flask import Flask, request, session, g, redirect, url_for, abort, \
     render_template, flash
from minitwit import app


@app.route("/databoard")
def databoard():
    return render_template('databoard.html')
