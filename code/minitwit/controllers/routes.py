# -*- coding: UTF-8 -*-
'''
Application: Blogging
Module: Controllers Routes
Description: Tasting Docker, Flask, Rancher
author: Adriano dos Santos Vieira <adriano.svieira at google.com>
character encoding: UTF-8

Copyright 2016 Adriano dos Santos Vieira

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
@license @see LICENCE
'''
import os
import time
from flask import Flask, request, session, g, redirect, url_for, abort, \
     render_template, flash
from minitwit import app


@app.route("/")
def index():
    return render_template('index.html')


@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        if request.form['username'] != app.config['USERNAME']:
            error = 'Invalid username'
        elif request.form['password'] != app.config['PASSWORD']:
            error = 'Invalid password'
        else:
            session['logged_in'] = True
            flash('You were logged in')
            return redirect(url_for('index'))
    return render_template('login.html', error=error)


@app.route('/logout')
def logout():
    session.pop('logged_in', None)
    flash('You were logged out')
    return redirect(url_for('index'))


@app.route("/about")
def about():
    try:
        f = open(os.path.dirname(os.path.abspath(__file__))+'/VERSION')
        app_version = f.readline()
        f.close()
    except IOError as e:
        app_version = 'unreleased developement'

    flash('Some messages goes here!')
    return render_template('about.html',
                           APP_VERSION=app_version,
                           datetime=time.strftime("%d/%h/%Y %H:%M:%S"),
                           container=os.uname()[1],
                           hosted=' '.join(os.uname()))
