# -*- coding: UTF-8 -*-
'''
Application: MiniTwit
Modulue: application starting point
Description: Tasting Docker, Flask, Rancher
author: Adriano dos Santos Vieira <adriano.svieira at google.com>
character encoding: UTF-8

Copyright 2016 Adriano dos Santos Vieira

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
@license @see LICENCE
'''
import logging
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand


app = Flask(__name__)
app.url_map.strict_slashes = False
app.config.from_object('config')
app.logger.setLevel(app.config['DEBUG_LEVEL'])

db = SQLAlchemy(app)
migrate = Migrate(app, db)

manager = Manager(app)
manager.add_command('db', MigrateCommand)

from minitwit.controllers import routes
from minitwit.models import tables
from minitwit.controllers import users
from minitwit.controllers import databoard
