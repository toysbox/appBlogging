# -*- mode: ruby -*-
# vi: set ft=ruby :
'''
/**
 * Virtualization
 *
 * Vagrantfile for Linux Virtualization clustering
 * plugins:
 * - vagrant-hosts
 * - vagrant-hostsupdater
 * - vagrant-proxyconf
 * @author Adriano Vieira <adriano.svieira at gmail.com>

   Copyright 2016 Adriano dos Santos Vieira

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 * @license @see LICENCE
 */
 '''

# Cluster nodes (default: 2)
CLUSTER_NODES = (ENV.key?('CLUSTER_NODES') ? ENV['CLUSTER_NODES'].to_i : 2)

Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://atlas.hashicorp.com/search.
  config.vm.box = "adrianovieira/centos7-docker1.12-GA"
  #config.vm.box_version = '1.1'
  #config.vm.box = "adrianovieira/debian8-docker1.12-GA"
  #config.vm.box = "centos/7"

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # config.vm.network "forwarded_port", guest: 80, host: 8080

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  # config.vm.network "private_network", ip: "192.168.33.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "../data", "/vagrant_data"

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  # config.vm.provider "virtualbox" do |vb|
  #   # Display the VirtualBox GUI when booting the machine
  #   vb.gui = true
  #
  #   # Customize the amount of memory on the VM:
  #   vb.memory = "1024"
  # end
  #
  # View the documentation for the provider you are using for more
  # information on available options.

  # Define a Vagrant Push strategy for pushing to Atlas. Other push strategies
  # such as FTP and Heroku are also available. See the documentation at
  # https://docs.vagrantup.com/v2/push/atlas.html for more information.
  # config.push.define "atlas" do |push|
  #   push.app = "YOUR_ATLAS_USERNAME/YOUR_APPLICATION_NAME"
  # end

  # Enable provisioning with a shell script. Additional provisioners such as
  # Puppet, Chef, Ansible, Salt, and Docker are also available. Please see the
  # documentation for more information about their specific syntax and use.
  # config.vm.provision "shell", inline: <<-SHELL
  #   apt-get update
  #   apt-get install -y apache2
  # SHELL

  config.vm.provider "virtualbox" do |virtualbox| # general Virtualbox.settings
    virtualbox.customize [ "modifyvm", :id, "--cpus", 1 ]
    virtualbox.customize [ "modifyvm", :id, "--memory", 2048 ]
    virtualbox.customize [ "modifyvm", :id, "--name", 'vm.hacklab' ]
    virtualbox.customize [ "modifyvm", :id, "--groups", "/cluster" ]
  end # end Virtualbox.settings

  config.vm.provision "docker-setup-proxy", type: "shell",
          path: "scripts/docker-setup-proxy.sh"

  if Vagrant.has_plugin?("vagrant-hosts")
    config.vm.provision :hosts do |provisioner|
      provisioner.add_localhost_hostnames = true
      provisioner.autoconfigure = true
      provisioner.sync_hosts = true
      provisioner.add_host '192.168.33.100', ['rancherserver', 'rancherserver.hacklab', 'cdn-server.hacklab']
    end
  end

  # cluster Master
  cluster_master_ip_pvt_net = '192.168.33.100'
  config.vm.define "appBlogging-cluster_master" do |master|  # define-VM
    master.vm.hostname = "appBlogging.hacklab"
    if Vagrant.has_plugin?("vagrant-hostsupdater")
      master.hostsupdater.aliases = ['rancherserver', 'rancherserver.hacklab', 'cdn-server.hacklab']
    end

    master.vm.network "private_network", ip: cluster_master_ip_pvt_net #, virtualbox__intnet: "cluster"
    master.vm.provider "virtualbox" do |virtualbox| # Virtualbox.settings
      virtualbox.customize [ "modifyvm", :id, "--cpus", 2 ]
      virtualbox.customize [ "modifyvm", :id, "--memory", 2048 ]
      virtualbox.customize [ "modifyvm", :id, "--name", "master.hacklab" ]
    end # end Virtualbox.settings
  end # end-of-define-VM cluster Master

  # cluster Hosts
  ipv4 = cluster_master_ip_pvt_net.split('.')
  node_id = 1

  (1..(CLUSTER_NODES)).each do |node_id|
    config.vm.define "appBlogging-node-#{node_id}" do |node|  # define-VM
      node.vm.hostname = "appBlogging-node-#{node_id}.hacklab"

      node_ipv4 = [ipv4[0], ipv4[1],ipv4[2], (ipv4[3].to_i+node_id)>=250?(ipv4[3].to_i-node_id):ipv4[3].to_i+node_id ].join('.')
      node.vm.network "private_network", ip: node_ipv4 #, virtualbox__intnet: "cluster"

      node.vm.provider "virtualbox" do |virtualbox| # Virtualbox.settings
        virtualbox.customize [ "modifyvm", :id, "--name", "appBlogging-node-#{node_id}.hacklab" ]
      end # end Virtualbox.settings
    end # end-of-define-VM
  end # end-of-define-VM-loop node_id

end