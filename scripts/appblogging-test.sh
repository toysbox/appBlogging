#!/bin/bash -x

# running docker tests
# docker run -i \
#            -e NO_PROXY='localhost,.hacklab,192.168.33.0/24' \
#            -e https_proxy=http://10.122.19.137:5865/ \
#            -e http_proxy=http://10.122.19.137:5865/ \
#
#            python:2.7 /bin/bash < build_script.sh
# or
#            python:3.5 /bin/bash < build_script.sh

#git clone -b 9-contacts-tests https://gitlab.com/toysbox/appBlogging.git /builds/toysbox/appBlogging

# test contacts
cd  /code/contacts

python setup.py test
